package ru.tsc.afelofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.model.IUserService;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.service.ConnectionService;
import ru.tsc.felofyanov.tm.service.PropertyService;
import ru.tsc.felofyanov.tm.service.model.UserService;

public class AbstractTest {

    @NotNull
    protected static IConnectionService CONNECTION_SERVICE;

    @NotNull
    protected static IPropertyService PROPERTY_SERVICE;

    @NotNull
    protected static IUserService userService;

    @NotNull
    protected User testUser;

    @BeforeClass
    public static void initConnectionService() {
        PROPERTY_SERVICE = new PropertyService();
        CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);
    }

    @AfterClass
    public static void closeConnectionService() {
        CONNECTION_SERVICE.close();
    }

    @Before
    public void init() {
        userService = new UserService(CONNECTION_SERVICE, PROPERTY_SERVICE);
        testUser = userService.create("testUser", "testUser");
    }

    @After
    public void close() {
        userService.removeByLogin("testUser");
    }
}
